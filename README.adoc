= FreshMarker

A simple, small but powerful template engine based loosely on the _FreeMarker_ syntax. _FreshMarker_ is implemented in Java 17 (Java 21 since version 1.0.0) and supports the `java.time` API and Records.


You can find a detailed description of all _Freshmarker_ features in the https://schegge.gitlab.io/freshmarker/[manual].

The https://gitlab.com/schegge/freshmarker/-/blob/develop/RELEASE_NOTES.adoc[release notes] contain changes to the template engine since version 1.4.6.

Some in-depth articles on _Freshmarker_ can be found on https://schegge.de/category/freshmarker/[schegge.de].

== Example Benchmarks

|===
.8+|image:src/main/asciidoc/benchmark.png[float="right"] |mustache | 0.9.11
|freemarker | 2.3.23
|velocity | 1.7
|thymeleaf | 2.1.4.RELEASE
|trimou | 1.8.2.Final
|freshmarker | 1.6.6
|===

== Usage

[source,xml]
----
<dependency>
include::pom.xml[lines=7..9]
</dependency>
----

== Examples

.Simple Example
[source,java]
----
Configuration configuration = new Configuration();
Template template = configuration.builder().getTemplate("test", "temporal=${temporal}, path=${path.size}");
String result = template.process(Map.of("temporal", LocalTime.of(12, 30), "path", Path.of("text.pdf")));
----

.List with loop Variable
[source,java]
----
Template template = configuration.builder().getTemplate("ancestors-loop",
    """
    <#list ancestors as ancestor with loop>
      ${loop?index}. ${ancestor.givenname} ${ancestor.surename}
    </#list>
    """);
String result = template.process(Map.of("ancestors", ancestorService.ancestorsByFamily("Kaiser")));
----

.Partial reduction of a Template
[source,java]
----
Template template = configuration.builder().getTemplate("ancestors-loop",
    """
    <h1>$title</h1>
    <#list ancestors as ancestor with loop>
      ${loop?index}. ${ancestor.givenname} ${ancestor.surename}
    </#list>
    """);
Template reduced = template.reduce("title", "Genealogy List");
String result = reduced.process(Map.of("ancestors", ancestorService.ancestorsByFamily("Kaiser")));
----
