package org.freshmarker.core;

public class UnsupportedDataTypeException extends ProcessException {

  public UnsupportedDataTypeException(String message) {
    super(message);
  }
}
