package org.freshmarker.core.model;

public interface TemplateBooleanExpression extends TemplateExpression {

  TemplateObject not();
}
