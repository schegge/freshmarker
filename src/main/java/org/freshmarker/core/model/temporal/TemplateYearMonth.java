package org.freshmarker.core.model.temporal;

import org.freshmarker.core.model.primitive.TemplatePrimitive;

import java.time.YearMonth;

public class TemplateYearMonth extends TemplatePrimitive<YearMonth> {
    public TemplateYearMonth(YearMonth value) {
        super(value);
    }
}
