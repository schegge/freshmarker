package org.freshmarker.core.model.temporal;

import org.freshmarker.core.model.primitive.TemplatePrimitive;

import java.time.MonthDay;

public class TemplateMonthDay extends TemplatePrimitive<MonthDay> {
    public TemplateMonthDay(MonthDay value) {
        super(value);
    }
}
