package org.freshmarker.core.model.primitive;
import ftl.Token;
import org.freshmarker.core.ProcessContext;
import org.freshmarker.core.model.TemplateObject;
import org.freshmarker.core.model.version.Version;

public class TemplateVersion extends TemplatePrimitive<Version> {
    public TemplateVersion(String value) {
        super(Version.byString(value));
    }

    public TemplateNumber major() {
        return TemplateNumber.of(getValue().major());
    }

    public TemplateNumber minor() {
        return TemplateNumber.of(getValue().minor());
    }

    public TemplateNumber patch() {
        return TemplateNumber.of(getValue().patch());
    }

    public TemplateBoolean isBefore(TemplateVersion value) {
        return TemplateBoolean.from(getValue().compareTo(value.getValue()) < 0);
    }

    public TemplateBoolean isEqual(TemplateVersion value) {
        return TemplateBoolean.from(getValue().equals(value.getValue()));
    }

    public TemplateBoolean isAfter(TemplateVersion value) {
        return TemplateBoolean.from(getValue().compareTo(value.getValue()) > 0);
    }

    @Override
    public String toString() {
        return getValue().toString();
    }

    @Override
    public boolean relation(Token.TokenType operator, TemplateObject operand, ProcessContext context) {
        TemplateVersion rightValue = operand.evaluate(context, TemplateVersion.class);
        return switch (operator) {
            case LT -> getValue().compareTo(rightValue.getValue()) < 0;
            case GT -> getValue().compareTo(rightValue.getValue()) > 0;
            case LTE -> getValue().compareTo(rightValue.getValue()) <= 0;
            case GTE, UNICODE_GTE -> getValue().compareTo(rightValue.getValue()) >= 0;
            default -> super.relation(operator, operand, context);
        };
    }
}
