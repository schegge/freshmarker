package org.freshmarker.core.ftl;

import org.freshmarker.core.model.TemplateObject;

public record ParameterHolder(String name, TemplateObject defaultValue) {

}
