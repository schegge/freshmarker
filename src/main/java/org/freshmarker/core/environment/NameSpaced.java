package org.freshmarker.core.environment;

public record NameSpaced(String namespace, String name) {
}
