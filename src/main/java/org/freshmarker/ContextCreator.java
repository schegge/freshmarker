package org.freshmarker;

import org.freshmarker.core.ProcessContext;
import org.freshmarker.core.directive.UserDirective;
import org.freshmarker.core.environment.NameSpaced;

import java.io.Writer;
import java.util.Map;

interface ContextCreator {
    ProcessContext createContext(Map<String, Object> dataModel, Writer writer, Map<NameSpaced, UserDirective> userDirectives);
}
