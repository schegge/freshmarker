package org.freshmarker.core.ftl;

import ftl.ParseException;
import org.freshmarker.Configuration;
import org.freshmarker.Template;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ExistsInterpolationTest {
    private Configuration configuration;

    @BeforeEach
    void setUp() {
        configuration = new Configuration();
    }

    @ParameterizedTest
    @CsvSource({
            "test: ${test??},test: yes",
            "test: ${test2??},test: no",
            "test: ${(test2.map)??},test: no",
    })
    void exists(String templateSource, String expected) throws ParseException {
        Template template = configuration.builder().getTemplate("test", templateSource);
        assertEquals(expected, template.process(Map.of("test", "test")));
    }
}