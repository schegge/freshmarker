package org.freshmarker.core.ftl;

import ftl.ParseException;
import org.freshmarker.Configuration;
import org.freshmarker.TemplateBuilder;
import org.freshmarker.Template;
import org.freshmarker.core.ProcessContext;
import org.freshmarker.core.model.TemplateObject;
import org.freshmarker.core.model.primitive.TemplateString;
import org.freshmarker.core.model.primitive.TemplateVersion;
import org.freshmarker.core.plugin.PluginProvider;
import org.freshmarker.test.util.TemplateBuilderParameterResolver;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.time.LocalDate;
import java.util.Map;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(TemplateBuilderParameterResolver.class)
class BuiltInVariableTest {

    @ParameterizedTest
    @CsvSource({
            "test: ${.locale},test: de_DE",
            "test: ${.locale?lang},test: de",
            "test: ${.locale?language},test: de",
            "test: ${.locale?country},test: DE",
            "test: ${.lang},test: de",
            "test: ${.language},test: de",
            "test: ${.country},test: DE",
    })
    void builtInVariables(String templateSource, String expected, TemplateBuilder templateBuilder) throws ParseException {
        Template template = templateBuilder.getTemplate("test", templateSource);
        assertEquals(expected, template.process(Map.of()));
    }

    @ParameterizedTest
    @CsvSource({
            "test: ${'1.0.2'?version},test: 1.0.2",
            "test: ${'1.0.2'?version?major},test: 1",
            "test: ${'1.0.2'?version?minor},test: 0",
            "test: ${'1.0.2'?version?patch},test: 2",
            "test: ${'1.0.2'?version?is_before('1.0.3')},test: yes",
            "test: ${'1.0.2'?version?is_before('1.1.0')},test: yes",
            "test: ${'1.0.2'?version?is_before('2.0.0')},test: yes",
            "test: ${'1.0.2'?version?is_before(after)},test: yes",
            "test: ${'1.0.2'?version?is_equal('1.0.2')},test: yes",
            "test: ${'2.0.2'?version?is_after('1.0.0')},test: yes",
            "test: ${'1.1.2'?version?is_after('1.0.0')},test: yes",
            "test: ${'1.0.2'?version?is_after('1.0.0')},test: yes",
            "test: ${'1.0.2'?version?is_after('1.0.0'?version)},test: yes",
            "test: ${'1.0.2'?version?is_before('1.0.2')},test: no",
            "test: ${'1.0.2'?version?is_before('1.0.0')},test: no",
            "test: ${'1.0.2'?version?is_equal('1.0.3')},test: no",
            "test: ${'1.0.2'?version?is_equal('1.1.2')},test: no",
            "test: ${'1.0.2'?version?is_equal('2.0.2')},test: no",
            "test: ${'1.0.2'?version?is_after('1.1.0')},test: no",
            "test: ${'1.0.2'?version?is_after('1.0.2')},test: no",
            "test: ${version?is_before('1.0.0'?version)},test: no",
            "test: ${version?is_after(after)},test: no",
            "test: ${version < after},test: yes",
            "test: ${version <= after},test: yes",
            "test: ${after > version},test: yes",
            "test: ${after >= version},test: yes",
            "test: ${version > after},test: no",
            "test: ${version >= after},test: no",
            "test: ${after < version},test: no",
            "test: ${after <= version},test: no",
    })
    void version(String templateSource, String expected, TemplateBuilder templateBuilder) throws ParseException {
        Template template = templateBuilder.getTemplate("test", templateSource);
        assertEquals(expected, template.process(Map.of("version", new TemplateVersion("1.0.2"), "after", new TemplateVersion("1.1.0"))));
    }

    @Test
    void now(TemplateBuilder templateBuilder) throws ParseException {
        Template template = templateBuilder.getTemplate("test", "test: ${.now?date}");
        assertEquals("test: " + LocalDate.now(), template.process(Map.of()));
    }

    @ParameterizedTest
    @CsvSource({
            "${'1.0'?version}",
            "${.gonzo}",
            "${'1.6.3'?version?is_before(42)}"
    })
    void invalidVersion(String input, TemplateBuilder templateBuilder) throws ParseException {
        Template template = templateBuilder.getTemplate("test", input);
        Map<String, Object> model = Map.of();
        assertThrows(IllegalStateException.class, () -> template.process(model));
    }

    @Test
    void invalidVersion(TemplateBuilder templateBuilder) throws ParseException {
        Template template = templateBuilder.getTemplate("test", "${'1.6.3'?version?is_before}");
        Map<String, Object> model = Map.of();
        assertThrows(IllegalArgumentException.class, () -> template.process(model));
    }

    @Test
    void customBuiltInVariable() throws ParseException {
        Configuration configuration = new Configuration();
        configuration.registerPlugin(new PluginProvider() {
            @Override
            public void registerBuiltInVariableProviders(Map<String, Function<ProcessContext, TemplateObject>> providers) {
                providers.put("custom",  c -> new TemplateString("abraxas"));
            }
        });
        Template template = configuration.builder().getTemplate("test", "${.custom}");
        Map<String, Object> model = Map.of();
        assertEquals("abraxas", template.process(model));
    }
}